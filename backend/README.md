# publish+release.gradle

## Propeties
| Properties |Beschreibung| Beispiel |
|--|--|--|
|project.version|Version des Artefaktes| '1.0.3' |
|publishRepo |Name des Nexus Repositories| 'release' |
|nexusBasePath | Pfad zum verwendeten Nexus |'https://nexus.de' |
|projectname | Name des Projekts|'MyProject' | 
|group | Gruppe des Artefaktes|'de.group' | 