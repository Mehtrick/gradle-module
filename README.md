# Gradle-Module für Java Backends  

Dieses Projekt bietet einige Gradle-Skripte an die via dem [Gradle-URL-Cache Plugin](https://github.com/kageiit/gradle-url-cache-plugin) eingebunden werden können.

[![forthebadge](http://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)](http://forthebadge.com)

[![forthebadge](http://forthebadge.com/images/badges/built-with-grammas-recipe.svg)](http://forthebadge.com)