#API-Tools

##Swagger.gradle

In der build.gradle m�ssen folgende dependencies im Buildscript hinterlegt werden:

		classpath 'de.undercouch:gradle-download-task:3.2.0'
		classpath "com.kageiit:url-cache-plugin:1.0.0"
        classpath "gradle.plugin.org.hidetake:gradle-swagger-generator-plugin:2.4.2"
		classpath "com.diffplug.spotless:spotless-plugin-gradle:3.4.0"
		
Weiterhin muss im ext{} bereich im Buildscript eine Variable swaggerconfigpath definiert werden, die zur ben�tigten config.json leitet
Beispiel:
swaggerconfigpath="${projectDir}/gradle/swagger/config.json"